<?php

namespace App\Controller;

use App\Entity\Product;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class DefaultController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index(EntityManagerInterface $em)
    {
        $products = $em->getRepository(Product::class)->findAll();

        return $this->render('base.html.twig', ['products' => $products]);
    }

    /**
     * @Route("/live")
     */
    public function ping()
    {
        return new Response('pong');
    }
}
